from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.form import TodoListForm, TodoItemForm


def todo_list(request):
    todolists = TodoList.objects.all()
    context = {"todo_list": todolists}
    return render(request, "todos/list.html", context)


def todo_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todolist}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo)
    context = {"todo_object": todo, "form": form}
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/itemcreate.html", context)
